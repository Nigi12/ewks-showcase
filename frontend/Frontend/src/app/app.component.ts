import {Component, NgZone} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  eventSource: any = window['EventSource'];

  response: any = [0, 0, 0];
  time: any = 0;

  constructor(private http: HttpClient,private ngZone: NgZone) {
  }

  ngOnInit() {
    this.response.min = 0;
    this.response.max = 0;
    this.response.avg = 0;

  }

  getJavaF() {
    var start = new Date().getTime();
    return this.http.get('http://localhost:8080/java/functional')
      .subscribe(respo => {
        this.response = respo;
        this.time = new Date().getTime() - start;
      });
  }

  getJavaR() {
    var start = new Date().getTime();

    return this.http.get('http://localhost:8080/java/reactive')
      .subscribe(respo => {
        this.response = respo;
        this.time = new Date().getTime() - start;

      });
  }


  getJavaMW() {
    var start = new Date().getTime();

    const eventSource = new this.eventSource('http://localhost:8080/java/moving');

    eventSource.onmessage = event => {

      let data = JSON.parse(event.data);

      // $apply external (window.EventSource) event data
      console.log(event.data);
      this.response = data;

    };
  }

  getScala() {
    var start = new Date().getTime();

    return this.http.get('http://localhost:9000/scala/functional')
      .subscribe(respo => {
        this.response = respo;
        this.time = new Date().getTime() - start;

      });
  }

  getScala2() {
    var start = new Date().getTime();

    return this.http.get('http://localhost:9000/scala/functional-service')
      .subscribe(respo => {
        this.response = respo;
        this.time = new Date().getTime() - start;

      });
  }
}


