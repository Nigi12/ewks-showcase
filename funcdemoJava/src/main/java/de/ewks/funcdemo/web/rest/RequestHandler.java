package de.ewks.funcdemo.web.rest;


import de.ewks.funcdemo.domain.Response;
import de.ewks.funcdemo.repository.API_Collection;
import de.ewks.funcdemo.service.API_Service;
import de.ewks.funcdemo.service.FunctionalService;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;


@RestController
@RequestMapping("/java/")
public class RequestHandler {
    private final FunctionalService functionalService;
    private final API_Collection api_collection;


    @Autowired
    public RequestHandler(FunctionalService functionalService, API_Collection api_collection) {
        this.functionalService = functionalService;
        this.api_collection = api_collection;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value="/moving",produces=MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Response> getMovingAverage() {
        return Flux.from(functionalService.calculateWindowMoving().toFlowable(BackpressureStrategy.BUFFER));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/reactive")
    public Single<Response> getReactvie() {
        return functionalService.calculateComplete();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/functional")
    public Response getFunctional() {
        return functionalService.calculateStreamOnly();
    }

    @PostMapping("/api")
    public void addAPI(@RequestBody String adress) {
        api_collection.addApi(adress);
    }


}
