package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        int ran = (int) (Math.random()*100000);
        List<UUID> randomList = new ArrayList<>(ran);
        for(int i =0; i<ran;i++){
            randomList.add(i,UUID.randomUUID());
        }

        SpringApplication.run(DemoApplication.class, args);
    }
}
