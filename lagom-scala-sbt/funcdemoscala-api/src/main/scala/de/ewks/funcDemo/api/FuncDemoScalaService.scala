package de.ewks.funcDemo.api


import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}

trait FuncDemoScalaService extends Service {
  def functional(): ServiceCall[NotUsed, (Double, Double, Double)]
  def functional_service(): ServiceCall[NotUsed, (Double, Double, Double)]


  def storeApi(): ServiceCall[API_Information, Done]

  override final def descriptor: Descriptor = {
    import Service._
    named("funcDemoScala")
      .withCalls(
        pathCall("/scala/functional", functional _),
        pathCall("/scala/functional-service", functional_service _),
        pathCall("/scala/api", storeApi _)
      )
      /*
      .withTopics(
        topic(FuncdemoscalaService.TOPIC_NAME, greetingsTopic)
          .addProperty(
          KafkaProperties.partitionKeyStrategy,
          PartitionKeyStrategy[GreetingMessageChanged](_.name)
        )
      )
      */
      .withAutoAcl(true)
    // @formatter:on
  }
}

case class API_Information(url: String, endPointType: String)

object API_Information{
  implicit val format: Format[API_Information] = Json.format[API_Information]
}


